<?php
namespace Minds\Core\Feeds\Top;

use Minds\Traits\MagicAttributes;

class MetricsSync
{

    use MagicAttributes;

    private $guid;

    private $type;

    private $metric;

    private $count;

    private $period;

    private $synced;

}

